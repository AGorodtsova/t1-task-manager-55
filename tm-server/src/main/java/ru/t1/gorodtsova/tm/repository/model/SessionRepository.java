package ru.t1.gorodtsova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.model.ISessionRepository;
import ru.t1.gorodtsova.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        @NotNull final String jpql = "SELECT m FROM Session m";
        return entityManager.createQuery(jpql, Session.class).getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM Session";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM Session m WHERE m.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Session m";
        return entityManager.createQuery(jpql, Session.class)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Session m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getFirstResult();
    }

}
