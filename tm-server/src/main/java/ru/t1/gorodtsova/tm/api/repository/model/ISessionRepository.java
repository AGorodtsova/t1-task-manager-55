package ru.t1.gorodtsova.tm.api.repository.model;

import ru.t1.gorodtsova.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
