package ru.t1.gorodtsova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

    void removeOne(@NotNull String userId, @NotNull M model);

    void removeOneById(@NotNull String userId, @NotNull String id);

    boolean existsById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

}
