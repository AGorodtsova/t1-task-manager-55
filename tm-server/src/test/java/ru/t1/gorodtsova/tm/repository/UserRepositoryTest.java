package ru.t1.gorodtsova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.model.User;
import ru.t1.gorodtsova.tm.repository.dto.UserDtoRepository;
import ru.t1.gorodtsova.tm.service.ConnectionService;
import ru.t1.gorodtsova.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.gorodtsova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private IUserDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDtoRepository(entityManager);
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER_LIST);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_LIST, repository.findAll());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void set() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER_LIST1);
            Assert.assertEquals(USER_LIST1, repository.findAll());
            repository.set(USER_LIST2);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_LIST2, repository.findAll());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER_LIST1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_LIST1, repository.findAll());
            Assert.assertNotEquals(USER_LIST2, repository.findAll());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findOneById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            repository.add(USER2);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
            Assert.assertNotEquals(USER2, repository.findOneById(USER1.getId()));
            Assert.assertNull(repository.findOneById(ADMIN1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeOneById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            repository.add(USER2);
            repository.removeOneById(USER1.getId());
            entityManager.getTransaction().commit();
            Assert.assertFalse(repository.findAll().contains(USER1));
            Assert.assertTrue(repository.findAll().contains(USER2));
            Assert.assertThrows(ModelNotFoundException.class, () -> repository.removeOneById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeOne() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            repository.add(USER2);
            repository.removeOne(USER1);
            entityManager.getTransaction().commit();
            Assert.assertFalse(repository.findAll().contains(USER1));
            Assert.assertTrue(repository.findAll().contains(USER2));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER_LIST);
            Assert.assertEquals(USER_LIST, repository.findAll());
            repository.removeAll();
            entityManager.getTransaction().commit();
            Assert.assertTrue(repository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeAllCollection() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER_LIST);
            Assert.assertEquals(USER_LIST, repository.findAll());
            repository.removeAll(USER_LIST1);
            entityManager.getTransaction().commit();
            Assert.assertFalse(repository.findAll().contains(USER2));
            Assert.assertTrue(repository.findAll().contains(ADMIN1));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            entityManager.getTransaction().commit();
            Assert.assertTrue(repository.existsById(USER1.getId()));
            Assert.assertFalse(repository.existsById(USER2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findByLogin() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1, repository.findByLogin(USER1.getLogin()));
            Assert.assertNotEquals(USER1, repository.findByLogin(USER2.getLogin()));
            @NotNull final User user = new User();
            user.setLogin("login");
            Assert.assertNull(repository.findByLogin(user.getLogin()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findByEmail() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1, repository.findByEmail(USER1.getEmail()));
            Assert.assertNotEquals(USER1, repository.findByEmail(USER2.getEmail()));
            @NotNull final User user = new User();
            user.setEmail("user@company.ru");
            Assert.assertNull(repository.findByEmail(user.getEmail()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void isLoginExist() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            entityManager.getTransaction().commit();
            Assert.assertTrue(repository.isLoginExist(USER1.getLogin()));
            Assert.assertFalse(repository.isLoginExist(USER2.getLogin()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void isEmailExist() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            entityManager.getTransaction().commit();
            Assert.assertTrue(repository.isEmailExist(USER1.getEmail()));
            Assert.assertFalse(repository.isEmailExist(USER2.getEmail()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
