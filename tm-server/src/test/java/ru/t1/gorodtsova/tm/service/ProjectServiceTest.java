package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.api.service.dto.IProjectDtoService;
import ru.t1.gorodtsova.tm.api.service.dto.IUserDtoService;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.*;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.service.dto.ProjectDtoService;
import ru.t1.gorodtsova.tm.service.dto.UserDtoService;

import static ru.t1.gorodtsova.tm.constant.ProjectTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserDtoService userService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        userService.add(USER_LIST);
        projectService.add(PROJECT_LIST);
    }

    @After
    public void tearDown() {
        projectService.removeAll();
        userService.removeAll();
    }

    @Test
    public void add() {
        projectService.removeAll();
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.findAll().get(0).getId());
    }

    @Test
    public void addByUser() {
        projectService.removeAll();
        projectService.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.findAll().get(0).getId());
        Assert.assertEquals(USER1.getId(), projectService.findAll().get(0).getUserId());
        thrown.expect(UserIdEmptyException.class);
        projectService.add(null, USER1_PROJECT2);
    }

    @Test
    public void set() {
        Assert.assertFalse(projectService.findAll().isEmpty());
        projectService.set(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectService.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(PROJECT_LIST.size(), projectService.findAll().size());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectService.findAll(USER1.getId()).size());
        Assert.assertNotEquals(USER1_PROJECT_LIST.size(), projectService.findAll(USER2.getId()).size());
        thrown.expect(UserIdEmptyException.class);
        projectService.findAll("");
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.findOneById(USER1.getId(), USER1_PROJECT1.getId()).getId());

        thrown.expect(ModelNotFoundException.class);
        Assert.assertNull(projectService.findOneById(USER1.getId(), USER2_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.findOneById(null, USER1_PROJECT1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectService.findOneById(USER1.getId(), null);
    }

    @Test
    public void removeAll() {
        Assert.assertFalse(projectService.findAll().isEmpty());
        projectService.removeAll();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void removeAllByUserId() {
        Assert.assertFalse(projectService.findAll(USER1.getId()).isEmpty());
        projectService.removeAll(USER1.getId());
        Assert.assertTrue(projectService.findAll(USER1.getId()).isEmpty());
        thrown.expect(UserIdEmptyException.class);
        projectService.removeAll("");
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertTrue(projectService.existsById(USER1_PROJECT2.getId()));
        projectService.removeOneById(USER1.getId(), USER1_PROJECT2.getId());
        Assert.assertFalse(projectService.existsById(USER1_PROJECT2.getId()));

        thrown.expect(ModelNotFoundException.class);
        projectService.removeOneById(USER2.getId(), USER1_PROJECT1.getId());

        thrown.expect(UserIdEmptyException.class);
        projectService.removeOneById(null, USER1_PROJECT1.getId());

        thrown.expect(IdEmptyException.class);
        projectService.removeOneById(USER1.getId(), null);
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(projectService.existsById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(USER2.getId(), USER1_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.existsById(null, USER1_PROJECT1.getId());

        thrown.expect(IdEmptyException.class);
        projectService.existsById(USER1.getId(), null);
    }

    @Test
    public void createProjectName() {
        @NotNull final ProjectDTO project = projectService.create(USER1.getId(), "test project");
        Assert.assertEquals(project.getId(), projectService.findOneById(project.getId()).getId());
        Assert.assertEquals("test project", projectService.findOneById(project.getId()).getName());
        Assert.assertEquals(USER1.getId(), projectService.findOneById(project.getId()).getUserId());

        thrown.expect(UserIdEmptyException.class);
        projectService.create(null, USER1_PROJECT1.getName());

        thrown.expect(NameEmptyException.class);
        projectService.create(USER1.getId(), null);
    }

    @Test
    public void createProjectNameDescription() {
        @NotNull final ProjectDTO project = projectService.create(USER1.getId(), "test project", "test description");
        Assert.assertEquals(project.getId(), projectService.findOneById(project.getId()).getId());
        Assert.assertEquals("test project", projectService.findOneById(project.getId()).getName());
        Assert.assertEquals("test description", projectService.findOneById(project.getId()).getDescription());
        Assert.assertEquals(USER1.getId(), projectService.findOneById(project.getId()).getUserId());

        thrown.expect(UserIdEmptyException.class);
        projectService.create(null, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription());

        thrown.expect(NameEmptyException.class);
        projectService.create(USER1.getId(), null, USER1_PROJECT1.getDescription());

        thrown.expect(DescriptionEmptyException.class);
        projectService.create(USER1.getId(), "test project", null);
    }

    @Test
    public void updateById() {
        projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), "new name", "new description");
        Assert.assertEquals("new name", projectService.findOneById(USER1_PROJECT2.getId()).getName());
        Assert.assertEquals("new description", projectService.findOneById(USER1_PROJECT2.getId()).getDescription());

        thrown.expect(UserIdEmptyException.class);
        projectService.updateById(null, USER1_PROJECT2.getId(), "new name", "new description");

        thrown.expect(IdEmptyException.class);
        projectService.updateById(USER1.getId(), null, "new name", "new description");

        thrown.expect(NameEmptyException.class);
        projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), null, "new description");

        thrown.expect(ProjectNotFoundException.class);
        projectService.updateById(USER2.getId(), USER1_PROJECT2.getId(), "new name", "new description");
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertEquals(Status.NOT_STARTED, projectService.findOneById(USER1_PROJECT1.getId()).getStatus());
        projectService.changeProjectStatusById(USER1.getId(), USER1_PROJECT1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findOneById(USER1_PROJECT1.getId()).getStatus());

        thrown.expect(UserIdEmptyException.class);
        projectService.changeProjectStatusById(null, USER1_PROJECT1.getId(), Status.IN_PROGRESS);

        thrown.expect(IdEmptyException.class);
        projectService.changeProjectStatusById(USER1.getId(), null, Status.IN_PROGRESS);

        thrown.expect(ProjectNotFoundException.class);
        projectService.changeProjectStatusById(USER2.getId(), USER1_PROJECT1.getId(), Status.IN_PROGRESS);
    }

}
