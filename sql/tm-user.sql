-- Table: public.tm_user

-- DROP TABLE IF EXISTS public.tm_user;

CREATE TABLE IF NOT EXISTS public.tm_user
(
    id character varying(100) COLLATE pg_catalog."default" NOT NULL,
	login character varying(50) COLLATE pg_catalog."default",
	password character varying(200) COLLATE pg_catalog."default",
    email character varying(200) COLLATE pg_catalog."default",
    first_name character varying(100) COLLATE pg_catalog."default",
    last_name character varying(100) COLLATE pg_catalog."default",
    middle_name character varying(100) COLLATE pg_catalog."default",
    role character varying(50) COLLATE pg_catalog."default",
    locked boolean NOT NULL DEFAULT false,
    CONSTRAINT tm_user_pk PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_user
    OWNER to postgres;

COMMENT ON TABLE public.tm_user
    IS 'TASK MANAGER USER TABLE';