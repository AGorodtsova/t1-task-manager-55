package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataXmlLoadFasterXmlRequest;

@Component
public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from xml file";

    @NotNull
    private final String NAME = "data-load-xml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(getToken());
        domainEndpoint.loadDataXmlFasterXml(request);
    }

}
