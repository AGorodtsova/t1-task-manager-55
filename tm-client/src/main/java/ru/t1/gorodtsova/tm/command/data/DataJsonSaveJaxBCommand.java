package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataJsonSaveJaxBRequest;

@Component
public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data in json file";

    @NotNull
    private final String NAME = "data-save-json-jaxb";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(getToken());
        domainEndpoint.saveDataJsonJaxb(request);
    }

}
