package ru.t1.gorodtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Change status project by id";

    @NotNull
    private final String NAME = "project-change-status-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken(), id, status);
        projectEndpoint.changeProjectStatusById(request);
    }

}
