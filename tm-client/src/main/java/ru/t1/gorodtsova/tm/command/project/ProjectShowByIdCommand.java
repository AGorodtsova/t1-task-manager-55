package ru.t1.gorodtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;
import ru.t1.gorodtsova.tm.dto.request.project.ProjectShowByIdRequest;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Display project by id";

    @NotNull
    private final String NAME = "project-show-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken(), id);
        @Nullable final ProjectDTO project = projectEndpoint.showProjectById(request).getProject();
        showProject(project);
    }

}
