package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataBinaryLoadRequest;

@Component
public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from binary file";

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(getToken());
        domainEndpoint.loadDataBinary(request);
    }

}
