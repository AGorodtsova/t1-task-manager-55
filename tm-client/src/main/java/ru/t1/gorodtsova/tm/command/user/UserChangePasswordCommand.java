package ru.t1.gorodtsova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "Change password of current user";

    @NotNull
    private final String NAME = "change-user-password";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken(), password);
        userEndpoint.changePassword(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
