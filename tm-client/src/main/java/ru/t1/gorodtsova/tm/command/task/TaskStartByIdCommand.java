package ru.t1.gorodtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.task.TaskStartByIdRequest;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

@Component
public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Start task by id";

    @NotNull
    private final String NAME = "task-start-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getToken(), id);
        taskEndpoint.startTaskById(request);
    }

}
