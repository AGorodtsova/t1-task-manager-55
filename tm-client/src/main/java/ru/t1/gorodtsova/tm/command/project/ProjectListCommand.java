package ru.t1.gorodtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;
import ru.t1.gorodtsova.tm.dto.request.project.ProjectListRequest;
import ru.t1.gorodtsova.tm.enumerated.ProjectSort;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Show list projects";

    @NotNull
    private final String NAME = "project-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull ProjectListRequest request = new ProjectListRequest(getToken(), sort);
        @Nullable final List<ProjectDTO> projects = projectEndpoint.listProject(request).getProjects();
        int index = 1;
        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
