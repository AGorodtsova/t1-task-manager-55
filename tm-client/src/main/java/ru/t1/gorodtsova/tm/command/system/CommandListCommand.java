package ru.t1.gorodtsova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.api.model.ICommand;
import ru.t1.gorodtsova.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-cmd";

    @NotNull
    private final String DESCRIPTION = "Show command list";

    @NotNull
    private final String NAME = "commands";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (@Nullable final ICommand command : commands) {
            if (command == null) continue;
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
