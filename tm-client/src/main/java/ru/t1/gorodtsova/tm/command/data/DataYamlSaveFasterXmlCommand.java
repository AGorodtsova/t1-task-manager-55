package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataYamlSaveFasterXmlRequest;

@Component
public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data in yaml file";

    @NotNull
    private final String NAME = "data-save-yaml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(getToken());
        domainEndpoint.saveDataYamlFasterXml(request);
    }

}
