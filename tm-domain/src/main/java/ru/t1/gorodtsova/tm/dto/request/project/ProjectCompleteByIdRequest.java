package ru.t1.gorodtsova.tm.dto.request.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIdRequest;

@NoArgsConstructor
public final class ProjectCompleteByIdRequest extends AbstractIdRequest {

    public ProjectCompleteByIdRequest(@Nullable final String token, @Nullable final String id) {
        super(token, id);
    }

}
